use strict;
use warnings;

use CGI::Compile;
use CGI::Emulate::PSGI;
use File::Basename;

my $dir = dirname($0);
my $sub = CGI::Compile->compile("$dir/wiki.pl");
our $app = CGI::Emulate::PSGI->handler($sub);
