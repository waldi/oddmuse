#!/usr/bin/perl
use strict;
use warnings;

use HTTP::Server::Simple::PSGI;

our $app;

require 'wiki.psgi';

my $server = HTTP::Server::Simple::PSGI->new(8080);
$server->host('localhost');
$server->app($app);
$server->run;
